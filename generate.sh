#!/bin/bash
clear
##Functions##
saveFunction(){
echo "#!/bin/bash" >> ./human/$NAME.txt
echo "NAME='$NAME'" >> ./human/$NAME.txt
echo "AGE='$AGE'" >> ./human/$NAME.txt
echo "GENDER='$GENDER'" >> ./human/$NAME.txt
echo "GLASSES='$GLASSES'" >> ./human/$NAME.txt
echo "WEIGHT='$WEIGHT'" >> ./human/$NAME.txt
echo "LENGTH='$LENGTH'" >> ./human/$NAME.txt
echo "HAIR='$HAIR'" >> ./human/$NAME.txt
echo "FEET='$FEET'" >> ./human/$NAME.txt
echo "INCHES='$INCHES'" >> ./human/$NAME.txt
echo "STR='$STR'" >> ./human/$NAME.txt
echo "DEX='$DEX'" >> ./human/$NAME.txt
echo "CON='$CON'" >> ./human/$NAME.txt
echo "INT='$INT'" >> ./human/$NAME.txt
echo "WIS='$WIS'" >> ./human/$NAME.txt
echo "CHAR='$CHAR'" >> ./human/$NAME.txt
echo "JOB='$JOB'" >> ./human/$NAME.txt
if [ "$GENDER" == "female" ]; then
    echo "BREAST='$BREAST'" >> ./human/$NAME.txt
    echo "File saved at ./human/$NAME.txt"
fi
echo "PATH1='$PATH1'" >> ./human/$NAME.txt
echo "NPC1NAME='$NPC1NAME'" >> ./human/$NAME.txt
echo "NPC1GENDER='$NPC1GENDER'" >> ./human/$NAME.txt
echo "NPC1BREAST='$NPC1BREAST'" >> ./human/$NAME.txt
}

statsFunction(){
echo "Name: " $NAME
echo "Age:" $AGE
echo "Gender: " $GENDER
echo "$GLASSES"
echo "Weight: " $WEIGHT
echo "Hair: " $LENGTH " and " $HAIR
echo "Height: "$FEET "Feet" $INCHES "Inches"
echo "Job: " $JOB
echo "STR: " $STR
echo "DEX: " $DEX
echo "CON: " $CON
echo "INT: " $INT
echo "WIS: " $WIS
echo "CHAR: " $CHAR

if [ "$GENDER" == "female" ]; then
    case $BREAST in
    1) BREAST="A" ;;
    2) BREAST="B" ;;
    3) BREAST="C" ;;
    4) BREAST="D" ;;
    5) BREAST="DD" ;;
    6) BREAST="E" ;;
    esac
    echo "Breasts Size: " $BREAST
fi
}

##Beginning of running script##
read -p "would you like to load a file? y/n: " LOAD
clear

##Load if statement, should detect wether there is a human folder or not first##
if [ "$LOAD" == "y" ]; then
    if [ -d ./human ]; then
        echo "--Files currently available--"
        ls ./human
        read -p "please type the exact name of the character without .txt: " CHARACTER
        . ./human/$CHARACTER.txt
		clear
    else
        echo "There are currently no files to load or the human directory was moved!"
        exit
    fi
else

##Beginning of character creation##
    read -p "Please enter a name: " NAME
    read -p "Please enter gender male/female: " GENDER
    AGE=`shuf -i 17-40 -n 1`
    WEIGHT=`shuf -i 90-180 -n 1`
    HAIR=`shuf -i 1-6 -n 1`
    LENGTH=`shuf -i 1-4 -n 1`
    EYES=`shuf -i 1-4 -n 1`
    FEET=`shuf -i 4-6 -n 1`
    INCHES=`shuf -i 0-11 -n 1`
    BREAST=`shuf -i 1-6 -n 1`
    GLASSES=`shuf -i 0-1 -n 1`
    STR=`shuf -i 3-18 -n 1`
    DEX=`shuf -i 3-18 -n 1`
    CON=`shuf -i 3-18 -n 1`
    INT=`shuf -i 3-18 -n 1`
    WIS=`shuf -i 3-18 -n 1`
    CHAR=`shuf -i 3-18 -n 1`
    JOBROLL=`shuf -i 1-6 -n 1`
	NPC1NAME=`shuf -i 1-6 -n 1`
	NPC1GENDER=`shuf -i 1-2 -n 1`
	NPC1BREAST=`shuf -i 1-6 -n 1`
	NPC1HAIR=`shuf -i 1-6 -n 1`


fi

##The various astetic attributes based on random rolls##
case $JOBROLL in
    1) JOB="CEO" ;;
    2) JOB="Vice-President" ;;
    3) JOB="HumanResources" ;;
    4) JOB="ITManager" ;;
    5) JOB="TechSupport" ;;
    6) JOB="SalesFloorEmployee" ;;
esac

case $HAIR in
    1) HAIR="White" ;;
    2) HAIR="Black" ;;
    3) HAIR="Red" ;;
    4) HAIR="Blonde" ;;
    5) HAIR="Brunette" ;;
    6) HAIR="Grey" ;;
esac

case $LENGTH in
    1) LENGTH="Short" ;;
    2) LENGTH="Medium" ;;
    3) LENGTH="Long" ;;
    4) LENGTH="Super Long" ;;
esac

case $EYES in
    1) EYES="Brown" ;;
    2) EYES="Blue" ;;
    3) EYES="Green" ;;
    4) EYES="Hazel" ;;
esac

case $GLASSES in
    0) GLASSES="Eyes: $EYES and wears glasses" ;;
    1) GLASSES="Eye color: $EYES" ;;
esac

case $NPC1GENDER in
	1) NPC1GENDER="male" ;;
	2) NPC1GENDER="female" ;;
esac

if [ "NPC1GENDER" == "female" ]; then
	case $NPC1NAME in
		1) NPC1NAME="Alex" ;;
		2) NPC1NAME="Renee" ;;
		3) NPC1NAME="Laura" ;;
		4) NPC1NAME="Lina" ;;
		5) NPC1NAME="Angel" ;;
		6) NPC1NAME="Molly" ;;
	esac

	case $NPC1BREAST in
		1) NPC1BREAST="A" ;;
		2) NPC1BREAST="B" ;;
		3) NPC1BREAST="C" ;;
		4) NPC1BREAST="D" ;;
		5) NPC1BREAST="DD" ;;
		6) NPC1BREAST="E" ;;
	esac
else
	case $NPC1NAME in
		1) NPC1NAME="Bob" ;;
		2) NPC1NAME="George" ;;
		3) NPC1NAME="Fred" ;;
		4) NPC1NAME="Greg" ;;
		5) NPC1NAME="Tony" ;;
		6) NPC1NAME="Justin" ;;
	esac
fi

	case $NPC1HAIR in
		1) NPC1HAIR="White" ;;
		2) NPC1HAIR="Black" ;;
		3) NPC1HAIR="Red" ;;
		4) NPC1HAIR="Blonde" ;;
		5) NPC1HAIR="Brunette" ;;
		6) NPC1HAIR="Grey" ;;
	esac
##This is where it calls the stats and echos them back##
statsFunction

echo " "

##Flavor text, will add more to this later##
if [ "$GENDER" == "female" ]; then
    if [ "$JOB" == "CEO" ]; then
        if [ "$INT" == "3" ]; then
			echo "Demeanor:"
			echo " "
            echo "You stare blankly at the square box in front of you and scratch at your $HAIR hair. You know this box is supposed to do something. You smack it thinking it might help. Unfortunately it seems smacking it caused a piece to fall off. Picking it up you examine it and pull out a hammer. Knowing full well this will fix the problem."
			TDEMEANOR="1"
        elif [ "$INT" -le "14" ]; then
			echo "Demeanor:"
			echo " "
            echo "Being a woman, it's been hard to be in the lime light of the company, being that you're not particularly smart. Especially in the IT field. You think secretly the IT scum are making fun of you behind your back. You always just merely flip your $LENGTH $HAIR hair and turn your nose up at them as they walk by. Knowing full well they like the view as you walk away."
			TDEMEANOR="2"
        elif [ "$INT" -le "17" ]; then
			echo "Demeanor:"
			echo " "
            echo "Being that you're a woman and the owner of your own company. Not many think that you're smart, but the IT guys seem to have your back when it comes to questions. They're nice and respectful and you always seem to have the right questions and follow through with their advice. You may be no tech guru, but you sure as hell try."
			TDEMEANOR="3"
        else
			echo "Demeanor:"
			echo " "
            echo "It's been rough being a woman, and being extremely intelligent. Especially to the IT crowd. Not many women fills it's rosters. You made this company, and grew it from the ground up with barely any need of an IT crew. You hire them anyways though to carry out the tasks you deem necessary and they don't seem to really complain too much. They know you're not stupid and respect your decisions."
			TDEMEANOR="4"
        fi
    elif [ "$JOB" == "Vice-President" ]; then
        if [ "$INT" == "3" ]; then
			echo "Demeanor:"
			echo " "
            echo "completely and utter failure $JOB"
        elif [ "$INT" -le "14" ]; then
			echo "Demeanor:"
			echo " "
            echo "Dumb $JOB"
        elif [ "$INT" -le "17" ]; then
			echo "Demeanor:"
			echo " "
            echo "Semi-smart $JOB"
        else
			echo "Demeanor:"
			echo " "
            echo "Smart $JOB"
        fi
    elif [ "$JOB" == "HumanResources" ]; then
        if [ "$INT" == "3" ]; then
			echo "Demeanor:"
			echo " "
            echo "completely and utter failure $JOB"
        elif [ "$INT" -le "14" ]; then
			echo "Demeanor:"
			echo " "
            echo "Dumb $JOB"
        elif [ "$INT" -le "17" ]; then
			echo "Demeanor:"
			echo " "
            echo "Semi-smart $JOB"
        else
			echo "Demeanor:"
			echo " "
            echo "Smart $JOB"
        fi
    elif [ "$JOB" == "ITManager" ]; then
        if [ "$INT" == "3" ]; then
			echo "Demeanor:"
			echo " "
            echo "completely and utter failure $JOB"
        elif [ "$INT" -le "14" ]; then
			echo "Demeanor:"
			echo " "
            echo "Dumb $JOB"
        elif [ "$INT" -le "17" ]; then
			echo "Demeanor:"
			echo " "
            echo "Semi-smart $JOB"
        else
			echo "Demeanor:"
			echo " "
            echo "Smart $JOB"
        fi
    elif [ "$JOB" == "TechSupport" ]; then
        if [ "$INT" == "3" ]; then
			echo "Demeanor:"
			echo " "
            echo "completely and utter failure $JOB"
        elif [ "$INT" -le "14" ]; then
			echo "Demeanor:"
			echo " "
            echo "Dumb $JOB"
        elif [ "$INT" -le "17" ]; then
			echo "Demeanor:"
			echo " "
            echo "Semi-smart $JOB"
        else
			echo "Demeanor:"
			echo " "
            echo "Smart $JOB"
        fi
    elif [ "$JOB" == "SalesFloorEmployee" ]; then
        if [ "$INT" == "3" ]; then
			echo "Demeanor:"
			echo " "
            echo "completely and utter failure $JOB"
        elif [ "$INT" -le "14" ]; then
			echo "Demeanor:"
			echo " "
            echo "Dumb $JOB"
        elif [ "$INT" -le "17" ]; then
			echo "Demeanor:"
			echo " "
            echo "Semi-smart $JOB"
        else
			echo "Demeanor:"
			echo " "
            echo "Smart $JOB"
        fi
    fi
else

    if [ "$JOB" == "CEO" ]; then
        if [ "$INT" == "3" ]; then
			echo "Demeanor:"
			echo " "
            echo "You stare blankly at the square box in front of you and scratch at your $HAIR hair. You know this box is supposed to do something. You smack it thinking it might help. Unfortunately it seems smacking it caused a piece to fall off. Picking it up you examine it and pull out a hammer. Knowing full well this will fix the problem."
			TDEMEANOR="1"
        elif [ "$INT" -le "14" ]; then
			echo "Demeanor:"
			echo " "
            echo "You are the big cheese, the boss to end all bosses. Those IT guys and their thingamajiggers aren't anything to you. You rule this company with an iron fist and it doesn't matter a lick that you don't even know what a survore is."
			TDEMEANOR="2"
        elif [ "$INT" -le "17" ]; then
			echo "Demeanor:"
			echo " "
            echo "Looking at your email you notice you've lost connection to the exchange server. Knowing that's a pretty bad thing you call up the IT department just to double check they're on the case. Knowing full well they're probably already on it. You love your IT guys, they keep the world revolving."
			TDEMEANOR="3"
        else
			echo "Demeanor:"
			echo " "
            echo "You've pretty much single handedly programmed every server in the company and often times find yourself in the IT department just to get your hands dirty. The IT guys love you and often come to your own office just to chit chat. You always make sure the IT department is well taken care of and often make sure the rest of your employees go through your general computing classes."
			TDEMEANOR="4"
        fi
    elif [ "$JOB" == "Vice-President" ]; then
        if [ "$INT" == "3" ]; then
			echo "Demeanor:"
			echo " "
            echo "completely and utter failure $JOB"
        elif [ "$INT" -le "14" ]; then
			echo "Demeanor:"
			echo " "
            echo "Dumb $JOB"
        elif [ "$INT" -le "17" ]; then
			echo "Demeanor:"
			echo " "
            echo "Semi-smart $JOB"
        else
			echo "Demeanor:"
			echo " "
            echo "Smart $JOB"
        fi
    elif [ "$JOB" == "HumanResources" ]; then
        if [ "$INT" == "3" ]; then
			echo "Demeanor:"
			echo " "
            echo "completely and utter failure $JOB"
        elif [ "$INT" -le "14" ]; then
			echo "Demeanor:"
			echo " "
            echo "Dumb $JOB"
        elif [ "$INT" -le "17" ]; then
			echo "Demeanor:"
			echo " "
            echo "Semi-smart $JOB"
        else
			echo "Demeanor:"
			echo " "
            echo "Smart $JOB"
        fi
    elif [ "$JOB" == "ITManager" ]; then
        if [ "$INT" == "3" ]; then
			echo "Demeanor:"
			echo " "
            echo "completely and utter failure $JOB"
        elif [ "$INT" -le "14" ]; then
			echo "Demeanor:"
			echo " "
            echo "Dumb $JOB"
        elif [ "$INT" -le "17" ]; then
			echo "Demeanor:"
			echo " "
            echo "Semi-smart $JOB"
        else
			echo "Demeanor:"
			echo " "
            echo "Smart $JOB"
        fi
    elif [ "$JOB" == "TechSupport" ]; then
        if [ "$INT" == "3" ]; then
			echo "Demeanor:"
			echo " "
            echo "completely and utter failure $JOB"
        elif [ "$INT" -le "14" ]; then
			echo "Demeanor:"
			echo " "
            echo "Dumb $JOB"
        elif [ "$INT" -le "17" ]; then
			echo "Demeanor:"
			echo " "
            echo "Semi-smart $JOB"
        else
            echo "Smart $JOB"
        fi
    elif [ "$JOB" == "SalesFloorEmployee" ]; then
        if [ "$INT" == "3" ]; then
			echo "Demeanor:"
			echo " "
            echo "completely and utter failure $JOB"
        elif [ "$INT" -le "14" ]; then
			echo "Demeanor:"
			echo " "
            echo "Dumb $JOB"
        elif [ "$INT" -le "17" ]; then
			echo "Demeanor:"
			echo " "
            echo "Semi-smart $JOB"
        else
			echo "Demeanor:"
			echo " "
            echo "Smart $JOB"
        fi
    fi

fi

##Beginning of first path##
echo " "
echo "<-------- Story Time -------->"
if [ "LOAD" == "n" ]; then
	PATH1=`shuf -i 1-6 -n 1`
fi

##CEO First PATH##
if [ "$JOB" == "CEO" ]; then
	if [ "$PATH1" == "1" ]; then
		echo "Sitting in your office you comb back your $LENGTH $HAIR hair with a weary hand. It's been a long day at the office."
		if [ "$TDEMEANOR" == "1" ]; then
			echo "This box in front of you is making some wierd beeping sounds every time you try and turn it on. At least you think your trying to turn it on. You really have no idea, you know it's got to be the Tech Support departments fault though. They're always screwing with you."
		elif [ "$TDEMEANOR" == "2" ]; then
			echo "Those damned tech bastards have been screwing things up all damn day. First they wouldn't paint your black monitor white, and then they damn well insulted you! You begin to think of a way to get back at them."
		elif [ "$TDEMEANOR" == "3" ]; then
			echo "Leaning back you begin to think on what you can do. It really has been a long day and the tech deparment seems to have really came through for you today. $NPC1NAME in the Tech department could probably use a visit."
		elif [ "$TDEMEANOR" == "4" ]; then
			echo "A computer lay in front of you in pieces. You had told $NPC1NAME that you would fix it for them so they could get a little bit of a break from the constant grueling ticket queue. It seems as though this hard drive is fried. You'll probably have to go down to the tech department to get a new one."
		fi
	elif [ "$PATH1" == "2" ]; then
		echo "Sitting in your office you comb back your $LENGTH $HAIR hair with a weary hand. It's been a long day at the office."
		if [ "$TDEMEANOR" == "1" ]; then
			echo "This box in front of you is making some wierd beeping sounds every time you try and turn it on. At least you think your trying to turn it on. You really have no idea, you know it's got to be the Tech Support departments fault though. They're always screwing with you."
		elif [ "$TDEMEANOR" == "2" ]; then
			echo "Those damned tech bastards have been screwing things up all damn day. First they wouldn't paint your black monitor white, and then they damn well insulted you! You begin to think of a way to get back at them."
		elif [ "$TDEMEANOR" == "3" ]; then
			echo "Leaning back you begin to think on what you can do. It really has been a long day and the tech deparment seems to have really came through for you today. $NPC1NAME in the Tech department could probably use a visit."
		elif [ "$TDEMEANOR" == "4" ]; then
			echo "A computer lay in front of you in pieces. You had told $NPC1NAME that you would fix it for them so they could get a little bit of a break from the constant grueling ticket queue. It seems as though this hard drive is fried. You'll probably have to go down to the tech department to get a new one."
		fi
	elif [ "$PATH1" == "3" ]; then
		echo "Sitting in your office you comb back your $LENGTH $HAIR hair with a weary hand. It's been a long day at the office."
		if [ "$TDEMEANOR" == "1" ]; then
			echo "This box in front of you is making some wierd beeping sounds every time you try and turn it on. At least you think your trying to turn it on. You really have no idea, you know it's got to be the Tech Support departments fault though. They're always screwing with you."
		elif [ "$TDEMEANOR" == "2" ]; then
			echo "Those damned tech bastards have been screwing things up all damn day. First they wouldn't paint your black monitor white, and then they damn well insulted you! You begin to think of a way to get back at them."
		elif [ "$TDEMEANOR" == "3" ]; then
			echo "Leaning back you begin to think on what you can do. It really has been a long day and the tech deparment seems to have really came through for you today. $NPC1NAME in the Tech department could probably use a visit."
		elif [ "$TDEMEANOR" == "4" ]; then
			echo "A computer lay in front of you in pieces. You had told $NPC1NAME that you would fix it for them so they could get a little bit of a break from the constant grueling ticket queue. It seems as though this hard drive is fried. You'll probably have to go down to the tech department to get a new one."
		fi
	elif [ "$PATH1" == "4" ]; then
		echo "Sitting in your office you comb back your $LENGTH $HAIR hair with a weary hand. It's been a long day at the office."
		if [ "$TDEMEANOR" == "1" ]; then
			echo "This box in front of you is making some wierd beeping sounds every time you try and turn it on. At least you think your trying to turn it on. You really have no idea, you know it's got to be the Tech Support departments fault though. They're always screwing with you."
		elif [ "$TDEMEANOR" == "2" ]; then
			echo "Those damned tech bastards have been screwing things up all damn day. First they wouldn't paint your black monitor white, and then they damn well insulted you! You begin to think of a way to get back at them."
		elif [ "$TDEMEANOR" == "3" ]; then
			echo "Leaning back you begin to think on what you can do. It really has been a long day and the tech deparment seems to have really came through for you today. $NPC1NAME in the Tech department could probably use a visit."
		elif [ "$TDEMEANOR" == "4" ]; then
			echo "A computer lay in front of you in pieces. You had told $NPC1NAME that you would fix it for them so they could get a little bit of a break from the constant grueling ticket queue. It seems as though this hard drive is fried. You'll probably have to go down to the tech department to get a new one."
		fi
	elif [ "$PATH1" == "5" ]; then
		echo "Sitting in your office you comb back your $LENGTH $HAIR hair with a weary hand. It's been a long day at the office."
		if [ "$TDEMEANOR" == "1" ]; then
			echo "This box in front of you is making some wierd beeping sounds every time you try and turn it on. At least you think your trying to turn it on. You really have no idea, you know it's got to be the Tech Support departments fault though. They're always screwing with you."
		elif [ "$TDEMEANOR" == "2" ]; then
			echo "Those damned tech bastards have been screwing things up all damn day. First they wouldn't paint your black monitor white, and then they damn well insulted you! You begin to think of a way to get back at them."
		elif [ "$TDEMEANOR" == "3" ]; then
			echo "Leaning back you begin to think on what you can do. It really has been a long day and the tech deparment seems to have really came through for you today. $NPC1NAME in the Tech department could probably use a visit."
		elif [ "$TDEMEANOR" == "4" ]; then
			echo "A computer lay in front of you in pieces. You had told $NPC1NAME that you would fix it for them so they could get a little bit of a break from the constant grueling ticket queue. It seems as though this hard drive is fried. You'll probably have to go down to the tech department to get a new one."
		fi
	elif [ "$PATH1" == "6" ]; then
		echo "Sitting in your office you comb back your $LENGTH $HAIR hair with a weary hand. It's been a long day at the office."
		if [ "$TDEMEANOR" == "1" ]; then
			echo "This box in front of you is making some wierd beeping sounds every time you try and turn it on. At least you think your trying to turn it on. You really have no idea, you know it's got to be the Tech Support departments fault though. They're always screwing with you."
		elif [ "$TDEMEANOR" == "2" ]; then
			echo "Those damned tech bastards have been screwing things up all damn day. First they wouldn't paint your black monitor white, and then they damn well insulted you! You begin to think of a way to get back at them."
		elif [ "$TDEMEANOR" == "3" ]; then
			echo "Leaning back you begin to think on what you can do. It really has been a long day and the tech deparment seems to have really came through for you today. $NPC1NAME in the Tech department could probably use a visit."
		elif [ "$TDEMEANOR" == "4" ]; then
			echo "A computer lay in front of you in pieces. You had told $NPC1NAME that you would fix it for them so they could get a little bit of a break from the constant grueling ticket queue. It seems as though this hard drive is fried. You'll probably have to go down to the tech department to get a new one."
		fi
	fi
fi
##Beginning of code Save block, should do some checks to see if human is present and such##
read -p "Would you like to save this? n/y: " SAVE

FILENAME="./human/$NAME.txt"
if [ -d ./human ]; then
    if [ "$SAVE" == "y" ]; then
        if [ -f "$FILENAME" ]; then
            read -p "This file exists, would you like to overwrite? y/n: " overwrite
            if [ "$overwrite" == "y" ]; then
                rm ./human/$NAME.txt
                saveFunction
                exit
            fi
        else
            saveFunction
        fi
    else
        exit
    fi
else
    mkdir ./human
    saveFunction
fi